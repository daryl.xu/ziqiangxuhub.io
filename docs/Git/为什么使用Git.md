# 为什么使用 `Git`

`Git` 是一个非常流行的分布式的版本管理工具，性能优于 `SVN`（`SVN` 高度依赖于中央代码服务器）
[git官方中文手册](https://git-scm.com/book/zh/v2/%E8%B5%B7%E6%AD%A5-%E5%85%B3%E4%BA%8E%E7%89%88%E6%9C%AC%E6%8E%A7%E5%88%B6)
ps：有点中心化和去中心化的意思

## 扩展阅读

[用 `Git` 做自动部署](https://www.jianshu.com/p/821ff301cbed)
[变基与merge](https://git-scm.com/book/zh/v2/Git-%E5%88%86%E6%94%AF-%E5%8F%98%E5%9F%BA)