---
home: true
actionText: 看看Python
actionLink: /Python/
features:
- title: 许自强的技术博客
  details: 记录杂七杂八的技术
footer: MIT Licensed | Copyright © 2019-present Daryl Xu
---
# 简短的自我介绍

入了计算机的坑，再也爬不出来了。
目前从事医疗影像和数据管理方面的软件开发工作，对WEB开发、Linux桌面、
计算机视觉比较感兴趣。
[我的`GitHub`](https://github.com/ziqiangxu)
